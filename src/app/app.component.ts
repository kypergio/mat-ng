import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {FormArray, FormBuilder, Validators} from '@angular/forms';


export interface Player {
  firstName: string;
  lastName: string;
  phoneNumber: number;
  country: string;
  city: string;
  address: string;
  state: string;
  zipCode: number;
  birthDate: string;
  height: number;
  weight: number;
  referencedBy: string;
  spouseFullName: string;
  emergencyContact: string;
  emergencyContactRelationship: string;
  emergencyContactPhone: string;
}

const ELEMENT_DATA: Player[] = [
  {
    firstName: "Cesar Giovanni",
    lastName: 'Villaneuva Zamora',
    phoneNumber: 5583036711,
    country: 'Mexico',
    city: "Mexico",
    address: "Reyes Acozac",
    state: "Mexico",
    zipCode: 55757,
    birthDate: "1995-01-19",
    height: 180,
    weight: 80,
    referencedBy: "kypergio",
    spouseFullName: "Cesar Giovanni Villanueva Z.",
    emergencyContact: "Rosalvo",
    emergencyContactRelationship: "parent",
    emergencyContactPhone: "5543845305"
  },
  {
    firstName: "Verónica",
    lastName: 'Rincón Gutiérrez',
    phoneNumber: 5593056715,
    country: 'Mexico',
    city: "Mexico",
    address: "Tecamac",
    state: "Mexico",
    zipCode: 55757,
    birthDate: "1990-11-24",
    height: 170,
    weight: 60,
    referencedBy: "veromoss",
    spouseFullName: "Verónica R.",
    emergencyContact: "Rosalvo",
    emergencyContactRelationship: "parent",
    emergencyContactPhone: "5543845305",
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  displayedColumns = ['firstName', 'lastName', 'phoneNumber', 'country', 'city', 'address', 'state',
    'zipCode', 'birthDate', 'height', 'weight', 'referencedBy', 'spouseFullName', 'emergencyContact',
    'emergencyContactRelationship', 'emergencyContactPhone', 'actions'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  @ViewChild(MatSort) sort: MatSort;

  // Form
  form: any;
  // Flags to know if a item is been saved
  saving: any = [];
  // Flags to know if a item is been deleted
  deleting: any = [];
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    // Set the sortable skill in the datasource
    this.dataSource.sort = this.sort;
    // Make the form
    this.form = this.fb.group({
      players: this.fb.array([])
    });

    for (let i = 0; i < ELEMENT_DATA.length; i++) {
      const itemForm = {};
      for (const key in ELEMENT_DATA[i]) {
        itemForm[key] = [ELEMENT_DATA[i][key] , Validators.required];
      }
      this.playerForm.push(this.fb.group(itemForm));
      this.saving.push(false);
      this.deleting.push(false);
    }
  }
  get playerForm() {
    return this.form.get('players') as FormArray;
  }
  update(element: Player, index: any) {
    if (!this.playerForm.controls[index].dirty) {
      // If any field was modified, don't do anything
      return;
    } else {
      // If something was modified and needs to be saved
      // This is to show the spinner
      this.saving[index] = true;
      // Here goes your service call or whatever you need to save data into DB
      // element parameter is the Player instance
      // I'm simulating only a setTimeout for 1.5 second
      setTimeout(() => {
        // To hide the spinner
        this.saving[index] = false;
        this.playerForm.controls[index].markAsPristine();
      }, 1500);
    }
  }
  delete(index: any) {
    // This is to show the spinner
    this.deleting[index] = true;
    // Here goes your service call to delete data in the DB
    // I'm only simulating a time out of 1.5 second
    setTimeout(() => {
      // Delete data from array
      ELEMENT_DATA.splice(index, 1);
      // Delete data from reactive form
      this.playerForm.removeAt(index);
      // Refresh Mat Table
      this.dataSource.data = ELEMENT_DATA;
      // This is to delete the flag in deleting flags
      this.deleting.splice(index, 1);
      // This is to delete the flag in saving flags
      this.saving.splice(index, 1);
    }, 1500);
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
